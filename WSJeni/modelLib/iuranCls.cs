﻿using System.Configuration;
using System.Data.SqlClient;
using objectLib;
using System.Data;
using System;
using System.Collections.Generic;

namespace modelLib
{
    public class iuranCls
    {
        string conCollmi = ConfigurationManager.ConnectionStrings["Collmi"].ConnectionString.ToString();
        //SqlTransaction trans;

        #region cekiuran
        public responseIuranObj cekIuran(String nokartu)
        {
            responseIuranObj objIuran = new responseIuranObj();
            dataIuranObj objH = new dataIuranObj();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conCollmi);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "Sp_cp_ws_getInfoTagihan_with_Bayar";
            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;
            //int no = 0;

            objM.code = "200"; 
            objM.message = "Ok";

            try
            {
                SqlDataReader drH;
                con.Open();
                cmdH.Parameters.Add(new SqlParameter("@nova", nokartu));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    if (drH["TotalTagihan"] != DBNull.Value) { objH.premi = drH["TotalTagihan"].ToString(); }
                    if (drH["StatusLunas"] != DBNull.Value) { objH.status = drH["StatusLunas"].ToString(); }
                    //no = no + 1;
                }

                drH.Close();
                cmdH.Parameters.Clear();

                
            }
            catch (Exception e)
            {
                //log file
                string method = logCls.GetMethodInfo();
                logCls.Logger(method, e.Message.ToString());
                //end log file

                objM.code = "401"; objM.message = e.Message.ToString();
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            objM.code = "200";
            objM.message = "Ok";

            objIuran.data = objH;
            objIuran.pesan = objM;

            

            /*if (objH.premi != null)
            //{
                objIuran.data = objH;
                
                objIuran.pesan = objM;
            //}
            /*else
            {
                objIuran.data = null;
                objM.code = "304"; objM.message = "Jenis Peserta Bukan PBPU atau BP";
                objIuran.pesan = objM;
            }*/

            return objIuran;
        }
        #endregion


    }
}
