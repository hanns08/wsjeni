﻿using objectLib;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;



namespace modelLib
{
    public class logCls
    {
        string Kon = ConfigurationManager.ConnectionStrings["Log"].ConnectionString.ToString();
        public static string strFolderLog = ConfigurationManager.AppSettings["FolderLog"];
        public static string strWebServiceName = ConfigurationManager.AppSettings["ServiceName"];
        SqlTransaction trans;

        #region UpdateLog
        public metaData update_Log(logObj objLog)
        {

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(Kon);
            SqlCommand cmd = new SqlCommand();
            //string id = "";
            cmd.CommandText = "usp_service_log";
            objM.code = "200";
            objM.message = "OK";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;

            try
            {
                con.Open();
                //trans = con.BeginTransaction();
                //cmd.Transaction = trans;
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new SqlParameter("@webservice", strWebServiceName));
                cmd.Parameters.Add(new SqlParameter("@from", objLog.request_from));
                cmd.Parameters.Add(new SqlParameter("@method", objLog.method));
                cmd.Parameters.Add(new SqlParameter("@urlservice", objLog.url));
                cmd.Parameters.Add(new SqlParameter("@methodid", objLog.methodId));
                cmd.Parameters.Add(new SqlParameter("@header", objLog.header));
                cmd.Parameters.Add(new SqlParameter("@request", objLog.request));
                cmd.Parameters.Add(new SqlParameter("@response", objLog.response));

                cmd.ExecuteNonQuery();
                //trans.Commit();
            }
            catch (SqlException e)
            {
                objM.code = "404";
                objM.message = e.Message;
                //log file
                string method = logCls.GetMethodInfo();
                logCls.Logger(method, e.Message.ToString());
                //end log file
                //trans.Rollback();
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            return objM;
        }
        #endregion


        public static void VerifyDir(string path)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                if (!dir.Exists)
                {
                    dir.Create();
                }
            }
            catch { }
        }

        public static void Logger(string activity, string message)
        {
            string path = strFolderLog;
            VerifyDir(path);
            string fileName = DateTime.Parse(DateTime.Now.ToString()).ToString("yyyyMMdd") + "_Logs.txt";
            //string fileName = DateTime.Parse(DateTime.Now.Day.ToString()).ToString("dd") + DateTime.Parse(DateTime.Now.Month.ToString()).ToString("MM") + DateTime.Parse(DateTime.Now.Year.ToString()).ToString("yyyy") + "_Logs.txt";
            try
            {
                if (!File.Exists(path + fileName))
                {
                    using (StreamWriter sw = File.CreateText(path + fileName))
                    {
                        sw.Write("\r\nLog " + activity + " : ");
                        sw.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                        sw.WriteLine($"  :{message}");
                        sw.WriteLine("-------------------------------");
                        sw.Close();
                    }
                }
                else
                {
                    using (StreamWriter sw = File.AppendText(path + fileName))
                    {
                        sw.Write("\r\nLog " + activity + " : ");
                        sw.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                        sw.WriteLine($"  :{message}");
                        sw.WriteLine("-------------------------------");
                        sw.Close();
                    }
                }
            }
            catch (Exception) { }



        }


        public static string GetMethodInfo()
        {
            string str = "";
            //// Get the current method namespace
            //str += "namespace name:"+System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Namespace +"\n";
            //// Get the current method class full name including the namespace
            //str += "namespace + class name:" +System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.FullName + "\n";
            //// Get the current class name
            //str += "class name:" + MethodBase.GetCurrentMethod().DeclaringType.Name+ "\n";
            //// Get the current method name
            //str += "method name:" + MethodBase.GetCurrentMethod().Name + "\n";
            //str += "\n";    
            StackTrace ss = new StackTrace(true);
            MethodBase mb = ss.GetFrame(1).GetMethod();
            //     // Get the parent method namespace
            //str += mb.DeclaringType.Namespace + "\n";
            //     // Get the parent method class name
            //str += mb.DeclaringType.Name + "\n";
            //     // Get the full name of the parent method class
            str += mb.DeclaringType.FullName + ".";
            // Get the parent method name
            str += mb.Name;
            return str;
        }

    }
}
