﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using objectLib;

namespace modelLib
{
    public class tiketCls
    {
        string conTiket = ConfigurationManager.ConnectionStrings["Tiket"].ConnectionString.ToString();
        string conBMC = ConfigurationManager.ConnectionStrings["BMC"].ConnectionString.ToString();

        #region statustiket
        public responseStatusTiketObj cekStatusTiket(string noTiket)
        {
            responseStatusTiketObj objTiket = new responseStatusTiketObj();
            dataStatusTiket objH = new dataStatusTiket();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conTiket);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "ws_rooc_searchbycode";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            try
            {
                SqlDataReader drH;
                con.Open();

                cmdH.Parameters.Add(new SqlParameter("@kodetiket", noTiket));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    if (drH["code"] != DBNull.Value) { objH.kodetiket = drH["code"].ToString(); }
                    if (drH["title"] != DBNull.Value) { objH.judultiket = drH["title"].ToString(); }
                    if (drH["description"] != DBNull.Value) { objH.deskripsi = drH["description"].ToString(); }
                    if (drH["tanggal"] != DBNull.Value) { objH.tanggalbuat = drH["tanggal"].ToString(); }
                    if (drH["sender"] != DBNull.Value) { objH.user = drH["sender"].ToString(); }
                    if (drH["senderUnit"] != DBNull.Value) { objH.unitkerja = drH["senderUnit"].ToString(); }
                    if (drH["email"] != DBNull.Value) { objH.email = drH["email"].ToString(); }
                    if (drH["detapplication"] != DBNull.Value) { objH.aplikasi = drH["detapplication"].ToString(); }
                    if (drH["detkategori"] != DBNull.Value) { objH.kategori = drH["detkategori"].ToString(); }
                    if (drH["detsubkategori"] != DBNull.Value) { objH.subkategori = drH["detsubkategori"].ToString(); }
                    if (drH["status"] != DBNull.Value) { objH.status = drH["status"].ToString(); }
                    if (drH["detcatatan"] != DBNull.Value) { objH.catatan = drH["detcatatan"].ToString(); }
                    if (drH["detunit"] != DBNull.Value) { objH.posisitiket = drH["detunit"].ToString(); }
                }

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch (Exception e)
            {
                objM.code = "401"; objM.message = e.ToString();
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (objH.kodetiket != null)
            {
                objTiket.data = objH;
                objM.code = "200"; objM.message = "OK";
                objTiket.pesan = objM;
            }
            else
            {
                objTiket.data = null;
                objM.code = "302"; objM.message = "Nomor Tiket Tidak Terdaftar";
                objTiket.pesan = objM;
            }

            return objTiket;
        }
        #endregion

        #region eskalasitiket
        public responseEskalasiTiket getEskalasiTiket(string noTiket)
        {
            responseEskalasiTiket objResp = new responseEskalasiTiket();

            //dataReferensiFaskes objRef = new dataReferensiFaskes();
            List<dataEskalasiTiket> objList = new List<dataEskalasiTiket>();

            dataResponseEskalasiTiket objData = new dataResponseEskalasiTiket();
            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conTiket);
            SqlCommand cmd = new SqlCommand();

            String jumlah = "";

            cmd.CommandText = "ws_rooc_escalationdtl";
            objM.code = "200";
            objM.message = "OK";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;

            try
            {
                con.Open();
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new SqlParameter("@kodetiket", noTiket));

                SqlDataReader drH;
                drH = cmd.ExecuteReader();

                while (drH.Read())
                {
                    dataEskalasiTiket objRef = new dataEskalasiTiket();

                    if (drH["code"] != DBNull.Value) { objRef.kodetiket = drH["code"].ToString(); }
                    if (drH["from"] != DBNull.Value) { objRef.dari = drH["from"].ToString(); }
                    if (drH["to"] != DBNull.Value) { objRef.ke = drH["to"].ToString(); }
                    if (drH["description"] != DBNull.Value) { objRef.catataneskalasi = drH["description"].ToString(); }
                    if (drH["datecreated"] != DBNull.Value) { objRef.tanggaleskalasi = drH["datecreated"].ToString(); }

                    objList.Add(objRef);
                }


                objData.list = objList;





                drH.Close();
                cmd.Parameters.Clear();
            }
            catch (Exception e)
            {

                objM.code = "500";
                objM.message = e.Message.ToString();

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (objData.list.Count != 0)
            {
                objResp.list = objData;
                objM.code = "200"; objM.message = "OK";
                objResp.pesan = objM;
            }
            else
            {
                objResp.list = null;
                objM.code = "302"; objM.message = "Nomor Tiket Tidak Terdaftar";
                objResp.pesan = objM;
            }


            return objResp;
        }
        #endregion

        #region penanganantiket
        public responsePenangananTiket getPenangananTiket(string noTiket)
        {
            responsePenangananTiket objResp = new responsePenangananTiket();

            //dataReferensiFaskes objRef = new dataReferensiFaskes();
            List<dataPenangananTiket> objList = new List<dataPenangananTiket>();

            dataResponsePenanganganTiket objData = new dataResponsePenanganganTiket();
            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conTiket);
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "ws_rooc_handlingdtl";
            objM.code = "200";
            objM.message = "OK";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;

            try
            {
                con.Open();
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new SqlParameter("@kodetiket", noTiket));

                SqlDataReader drH;
                drH = cmd.ExecuteReader();

                while (drH.Read())
                {
                    dataPenangananTiket objRef = new dataPenangananTiket();

                    if (drH["code"] != DBNull.Value) { objRef.kodetiket = drH["code"].ToString(); }
                    if (drH["detunit"] != DBNull.Value) { objRef.unitkerja = drH["detunit"].ToString(); }
                    if (drH["detuser"] != DBNull.Value) { objRef.user = drH["detuser"].ToString(); }
                    if (drH["description"] != DBNull.Value) { objRef.deskripsipenanganan = drH["description"].ToString(); }
                    if (drH["datecreated"] != DBNull.Value) { objRef.tanggalpenanganan = drH["datecreated"].ToString(); }

                    objList.Add(objRef);
                }


                objData.list = objList;


                objM.code = "200"; objM.message = "OK";


                drH.Close();
                cmd.Parameters.Clear();
            }
            catch (Exception e)
            {

                objM.code = "500";
                objM.message = e.Message.ToString();

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (objData.list.Count != 0)
            {
                objResp.list = objData;
                objM.code = "200"; objM.message = "OK";
                objResp.pesan = objM;
            }
            else
            {
                objResp.list = null;
                objM.code = "302"; objM.message = "Nomor Tiket Tidak Terdaftar";
                objResp.pesan = objM;
            }

            return objResp;
        }
        #endregion

        #region statusBMC
        public responseStatusBMCObj cekStatusBMC(string noTiket)
        {
            responseStatusBMCObj objResp = new responseStatusBMCObj();

            List<dataStatusBMC> objList = new List<dataStatusBMC>();
            dataResponseStatusBMC objData = new dataResponseStatusBMC();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conBMC);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "usp_bmc_read_ticket";

            objM.code = "200";
            objM.message = "Ok";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            int no = 0;

            try
            {
                SqlDataReader drH;
                con.Open();

                cmdH.Parameters.Add(new SqlParameter("@code", noTiket));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    dataStatusBMC objH = new dataStatusBMC();

                    if (drH["kode_user"] != DBNull.Value) { objH.kodeuser = drH["kode_user"].ToString(); }
                    if (drH["kode_eskalasi"] != DBNull.Value) { objH.kodeeskalasi = drH["kode_eskalasi"].ToString(); }
                    if (drH["title"] != DBNull.Value) { objH.judultiket = drH["title"].ToString(); }
                    if (drH["description"] != DBNull.Value) { objH.deskripsi = drH["description"].ToString(); }
                    if (drH["tanggal"] != DBNull.Value) { objH.tanggalbuat = drH["tanggal"].ToString(); }
                    if (drH["sender"] != DBNull.Value) { objH.user = drH["sender"].ToString(); }
                    if (drH["senderUnit"] != DBNull.Value) { objH.unitkerja = drH["senderUnit"].ToString(); }
                    if (drH["email"] != DBNull.Value) { objH.email = drH["email"].ToString(); }
                    if (drH["detapplication"] != DBNull.Value) { objH.aplikasi = drH["detapplication"].ToString(); }
                    if (drH["detkategori"] != DBNull.Value) { objH.kategori = drH["detkategori"].ToString(); }
                    if (drH["detsubkategori"] != DBNull.Value) { objH.subkategori = drH["detsubkategori"].ToString(); }
                    if (drH["status"] != DBNull.Value) { objH.status = drH["status"].ToString(); }
                    if (drH["detcatatan"] != DBNull.Value) { objH.catatan = drH["detcatatan"].ToString(); }
                    if (drH["detunit"] != DBNull.Value) { objH.posisitiket = drH["detunit"].ToString(); }

                    no = no + 1;
                    objList.Add(objH);

                }

                objData.list = objList;

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch (Exception e)
            {
                objM.code = "500"; objM.message = e.ToString();
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (!objM.code.Equals("500"))
            {
                if (no > 0)
                {
                    objResp.list = objData;
                    objM.code = "200"; objM.message = "OK";
                    //objResp.pesan = objM;
                }
                else
                {
                    objResp.list = null;
                    objM.code = "302"; objM.message = "Tiket Tidak Ditemukan";
                    //objResp.pesan = objM;
                }
            }

            objResp.pesan = objM;
            return objResp;
        }
        #endregion

        #region cekHandlingTiket
        public responseHandlingTiketObj cekHandlingTiket(string noTiket)
        {
            responseHandlingTiketObj objResp = new responseHandlingTiketObj();

            List<dataHandlingTiket> objList = new List<dataHandlingTiket>();
            dataResponseHandlingTiket objData = new dataResponseHandlingTiket();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conBMC);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "usp_bmc_handling_ticket";

            objM.code = "200";
            objM.message = "Ok";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            int no = 0;

            try
            {
                SqlDataReader drH;
                con.Open();

                cmdH.Parameters.Add(new SqlParameter("@code", noTiket));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    dataHandlingTiket objH = new dataHandlingTiket();

                    if (drH["code"] != DBNull.Value) { objH.kodetiket = drH["code"].ToString(); }
                    if (drH["from"] != DBNull.Value) { objH.dari = drH["from"].ToString(); }
                    if (drH["penanganan"] != DBNull.Value) { objH.deskripsipenanganan = drH["penanganan"].ToString(); }
                    if (drH["tanggal"] != DBNull.Value) { objH.tanggal = String.Format("{0:dd/MM/yyyy HH:mm:ss}", drH["tanggal"]).ToString(); }

                    no = no + 1;
                    objList.Add(objH);

                }

                objData.list = objList;

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch (Exception e)
            {
                objM.code = "500"; objM.message = e.ToString();
                string method = logCls.GetMethodInfo();
                logCls.Logger(method, e.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (!objM.code.Equals("500"))
            {
                if (no > 0)
                {
                    objResp.list = objData;
                    objM.code = "200"; objM.message = "OK";
                    //objResp.pesan = objM;
                }
                else
                {
                    objResp.list = null;
                    objM.code = "302"; objM.message = "Tiket Tidak Ditemukan";
                    //objResp.pesan = objM;
                }
            }

            objResp.pesan = objM;
            return objResp;
        }
        #endregion

        #region eskalasiBMC
        public responseEskalasiBMC getEskalasiBMC(string noTiket)
        {
            responseEskalasiBMC objResp = new responseEskalasiBMC();

            //dataReferensiFaskes objRef = new dataReferensiFaskes();
            List<dataEskalasiBMC> objList = new List<dataEskalasiBMC>();

            dataResponseEskalasiBMC objData = new dataResponseEskalasiBMC();
            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conTiket);
            SqlCommand cmd = new SqlCommand();

            int no = 0;

            cmd.CommandText = "usp_bmc_escalation_ticket";
            objM.code = "200";
            objM.message = "OK";

            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;

            try
            {
                con.Open();
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new SqlParameter("@code", noTiket));

                SqlDataReader drH;
                drH = cmd.ExecuteReader();

                while (drH.Read())
                {
                    dataEskalasiBMC objRef = new dataEskalasiBMC();

                    if (drH["code"] != DBNull.Value) { objRef.kodetiket = drH["code"].ToString(); }
                    if (drH["kepemilikan"] != DBNull.Value) { objRef.kepemilikan = drH["kepemilikan"].ToString(); }
                    if (drH["tanggal"] != DBNull.Value) { objRef.tanggaleskalasi = drH["tanggal"].ToString(); }
                    if (drH["total_waktu_penanganan"] != DBNull.Value) { objRef.waktupenanganan = drH["total_waktu_penanganan"].ToString(); }

                    no = no + 1;
                    objList.Add(objRef);
                }


                objData.list = objList;


                drH.Close();
                cmd.Parameters.Clear();
            }
            catch (Exception e)
            {

                objM.code = "500";
                objM.message = e.Message.ToString();

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (!objM.code.Equals("500"))
            {
                if (no > 0)
                {
                    objResp.list = objData;
                    objM.code = "200"; objM.message = "OK";
                    //objResp.pesan = objM;
                }
                else
                {
                    objResp.list = null;
                    objM.code = "302"; objM.message = "Tiket Tidak Ditemukan";
                    //objResp.pesan = objM;
                }
            }

            objResp.pesan = objM;
            return objResp;
        }
        #endregion
    }
}
