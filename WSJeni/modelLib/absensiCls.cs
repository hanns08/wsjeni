﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using objectLib;

namespace modelLib
{
    public class absensiCls
    {
        string conHCIS = ConfigurationManager.ConnectionStrings["HCIS"].ConnectionString.ToString();

        #region cekAbsensi
        public responseDataAbsensiObj cekAbsensi(string npp, int tahun, int bulan, int tanggal)
        {
            responseDataAbsensiObj objTiket = new responseDataAbsensiObj();
            dataAbsensi objH = new dataAbsensi();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conHCIS);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "hcis.sp_getLaporanAbsensiMobile_WSJeni";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            string namadepan = string.Empty;
            string namatengah = string.Empty;
            string namabelakang = string.Empty;

            try
            {
                SqlDataReader drH;
                con.Open();

                cmdH.Parameters.Add(new SqlParameter("@npp", npp));
                cmdH.Parameters.Add(new SqlParameter("@tahun", tahun));
                cmdH.Parameters.Add(new SqlParameter("@bulan", bulan));
                cmdH.Parameters.Add(new SqlParameter("@tanggal", tanggal));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    if (drH["npp"] != DBNull.Value) { objH.npp = drH["npp"].ToString(); }
                    if (drH["namadepan"] != DBNull.Value) { namadepan = drH["namadepan"].ToString(); }
                    if (drH["namatengah"] != DBNull.Value) { namatengah = drH["namatengah"].ToString(); }else { namatengah = ""; }
                    if (drH["namabelakang"] != DBNull.Value) { namabelakang = drH["namabelakang"].ToString(); }else { namabelakang = ""; }

                    
                    if(String.IsNullOrEmpty(namatengah)==true && String.IsNullOrEmpty(namabelakang)==true)
                    {
                        objH.nama = namadepan;
                    }
                    else if (String.IsNullOrEmpty(namatengah))
                    {
                        objH.nama = namadepan + " " + namabelakang;
                    }
                    else
                    {
                        objH.nama = namadepan + " " + namatengah + " " + namabelakang;
                    }

                    if (drH["datang_fwaktu"] != DBNull.Value) { objH.absenmasuk = drH["datang_fwaktu"].ToString(); }
                    if (drH["datang_sumber"] != DBNull.Value) { objH.absenmasukdari = drH["datang_sumber"].ToString(); }
                    if (drH["pulang_fwaktu"] != DBNull.Value) { objH.absenpulang = drH["pulang_fwaktu"].ToString(); }
                    if (drH["pulang_sumber"] != DBNull.Value) { objH.absenpulangdari = drH["pulang_sumber"].ToString(); }
                }

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch (Exception e)
            {
                objM.code = "401"; objM.message = "Laporan absensi dapat dilihat pada IHC Bestamo";
                string method = logCls.GetMethodInfo();
                logCls.Logger(method, e.Message);
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (!String.IsNullOrEmpty(objH.npp))
            {
                objTiket.data = objH;
                objM.code = "200"; objM.message = "OK";
                objTiket.pesan = objM;
            }
            else
            {
                objTiket.data = null;
                objM.code = "302"; objM.message = "NPP tidak ditemukan";
                objTiket.pesan = objM;
            }

            return objTiket;
        }
        #endregion
    }
}
