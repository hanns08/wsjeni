﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using objectLib;
using System.Data.SqlClient;

namespace modelLib
{
    public class consIDValidasi
    {
        private string strconReg = ConfigurationManager.ConnectionStrings["Registrasi"].ConnectionString;
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        public responseHeaderObj validasi(int consumerid, int methodid)
        {
            responseHeaderObj objHD = new responseHeaderObj();
            metaData objM = new metaData();
            headerObj obj = new headerObj();
            //string str = "usp_ws_ConsumerMethod_Lokasi_Limit";
            string str = "usp_ws_ConsumerMethod_Cek_ConsIdMethod";

            //objM.code = "201";
            //objM.message = "Consumer ID Belum Terdaftar";

            objM.code = "200";
            objM.message = "OK";

            con = new SqlConnection(strconReg);
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = str;
            try
            {
                con.Open();
                cmd.Parameters.Add(new SqlParameter("@cons_id", consumerid));
                cmd.Parameters.Add(new SqlParameter("@method_id", methodid));
                cmd.Parameters.Add(new SqlParameter("@get", 1));

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    if (dr["ConsumerID"] != DBNull.Value) { obj.consId = dr["ConsumerID"].ToString(); }
                    if (dr["ConsumerSecret"] != DBNull.Value) { obj.signature = dr["ConsumerSecret"].ToString(); }
                }

            }
            catch (Exception e)
            {
                objM.code = "404";
                objM.message = e.Message.ToString();
                //log file
                string method = logCls.GetMethodInfo();
                logCls.Logger(method, e.Message);
                //end log file

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                { con.Close(); }
            }

            objHD.header = obj;
            objHD.pesan = objM;

            return objHD;
        }

        public string validPPKConsumer(int consumerid)
        {
            string str = "select a.KDPPK from RefLokasiUserApp a inner join WS_Consumer b on b.LokasiID = a.LokasiID  where b.ConsumerID = @cons_id";
            string ret = "201";
            con = new SqlConnection(strconReg);
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = str;
            try
            {
                con.Open();
                cmd.Parameters.Add(new SqlParameter("@cons_id", consumerid));

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    if (dr["KDPPK"] != DBNull.Value) { ret = dr["KDPPK"].ToString(); }
                }
            }
            catch
            {
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                { con.Close(); }
            }
            return ret;
        }

        // using timestamp
        // using timestamp
        public responseHeaderObj validasi_2(int consumerid, int methodid, string timestamp, string signature)
        {
            responseHeaderObj objHD = new responseHeaderObj();
            metaData objM = new metaData();
            headerObj obj = new headerObj();
            string str = "usp_ws_ConsumerMethod_Cek_ConsIdMethod";

            //objM.code = "201";
            //objM.message = "Consumer ID Belum Terdaftar";

            objM.code = "200";
            objM.message = "OK";

            con = new SqlConnection(strconReg);
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = str;
            try
            {
                con.Open();
                cmd.Parameters.Add(new SqlParameter("@cons_id", consumerid));
                cmd.Parameters.Add(new SqlParameter("@method_id", methodid));
                cmd.Parameters.Add(new SqlParameter("@get", 2));

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                while (dr.Read())
                {
                    if (dr["ConsumerID"] != DBNull.Value) { obj.consId = dr["ConsumerID"].ToString(); }
                    if (dr["ConsumerSecret"] != DBNull.Value) { obj.consPwd = dr["ConsumerSecret"].ToString(); }
                    //if (dr["kdppk"] != DBNull.Value) { obj.kdPpk = dr["kdppk"].ToString(); }
                    obj.timestamp = timestamp;
                    obj.signature = signature;
                    obj.methodid = methodid.ToString();
                }

            }
            catch (Exception e)
            {
                objM.code = "404";
                objM.message = e.Message.ToString();
                //log file
                string method = logCls.GetMethodInfo();
                logCls.Logger(method, e.Message);
                //end log file

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                { con.Close(); }
            }

            objHD.header = obj;
            objHD.pesan = objM;

            return objHD;
        }
    }
}
