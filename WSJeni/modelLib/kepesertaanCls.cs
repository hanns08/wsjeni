﻿using System.Configuration;
using System.Data.SqlClient;
using objectLib;
using System.Data;
using System;
using System.Collections.Generic;

namespace modelLib
{
    public class kepesertaanCls
    {
        string conKepser = ConfigurationManager.ConnectionStrings["Kepesertaan"].ConnectionString.ToString();

        #region basepeserta
        public responseBaseKepesertaanObj cekBasePeserta(string nokartu)
        {
            responseBaseKepesertaanObj objKepesertaan = new responseBaseKepesertaanObj();
            dataBaseKepesertaanObj objH = new dataBaseKepesertaanObj();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conKepser);
            SqlCommand cmdH = new SqlCommand();

            int cek = 0;
            if (nokartu.Length.Equals(13))
            {
                cmdH.CommandText = "USP_DATNKAPST_NOKAPST_BASE";
                cek = 1;
            }
            else
            {
                cmdH.CommandText = "USP_DATNKAPST_NIK";
                cek = 2;
            }

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            try
            {
                SqlDataReader drH;
                con.Open();
                if (cek.Equals(1))
                {
                    cmdH.Parameters.Add(new SqlParameter("@PNOKA", nokartu));
                }
                else
                {
                    cmdH.Parameters.Add(new SqlParameter("@PNIK", nokartu));
                }
                

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    if (drH["KDJNSPESERTA"] != DBNull.Value) { objH.kdjnspeserta = drH["KDJNSPESERTA"].ToString(); }
                    if (drH["NOKAPST"] != DBNull.Value) { objH.nokapst = drH["NOKAPST"].ToString(); }
                    //no = no + 1;
                }

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch
            {
                objM.code = "401"; objM.message = "Transaksi Tidak Dapat Diproses, Silahkan Dicoba Kembali";
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (objH.kdjnspeserta != null)
            {
                objKepesertaan.data = objH;
                objM.code = "200"; objM.message = "OK";
                objKepesertaan.pesan = objM;
            }
            else
            {
                objKepesertaan.data = null;
                objM.code = "302"; objM.message = "Peserta Tidak Terdaftar";
                objKepesertaan.pesan = objM;
            }

            return objKepesertaan;
        }
        #endregion

        #region cekpeserta
        public responseStatusKepesertaanObj cekStatus(string nokartu)
        {
            responseStatusKepesertaanObj objKepesertaan = new responseStatusKepesertaanObj();
            dataStatusKepesertaanObj objH = new dataStatusKepesertaanObj();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conKepser);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "JENI_GETPESERTA_BY_NOKA_NIK";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            try
            {
                SqlDataReader drH;
                con.Open();
                if (nokartu.Length.Equals(13))
                {
                    cmdH.Parameters.Add(new SqlParameter("@CODE", "1"));
                }
                else
                {
                    cmdH.Parameters.Add(new SqlParameter("@CODE", "3"));
                }
               
                cmdH.Parameters.Add(new SqlParameter("@NOKA", nokartu));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    if (drH["NMPST"] != DBNull.Value) { objH.nmPeserta = drH["NMPST"].ToString(); }
                    if (drH["JKPST"] != DBNull.Value) { objH.jnskelPeserta = drH["JKPST"].ToString(); }
                    if (drH["TGLLHRPST"] != DBNull.Value) { objH.tglLhrPeserta = Convert.ToDateTime(drH["TGLLHRPST"]).ToString("yyyy-MM-dd"); }
                    if (drH["NMSTATUSPST"] != DBNull.Value) { objH.nmStatusPeserta = drH["NMSTATUSPST"].ToString(); }
                    if (drH["JNSPESERTA"] != DBNull.Value) { objH.jnsPeserta = drH["JNSPESERTA"].ToString(); }
                    if (drH["KDKELAS"] != DBNull.Value) { objH.klsPeserta = drH["KDKELAS"].ToString(); }
                    if (drH["NMPPK"] != DBNull.Value) { objH.nmPPK = drH["NMPPK"].ToString(); }
                    //no = no + 1;
                }

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch
            {
                objM.code = "401"; objM.message = "Transaksi Tidak Dapat Diproses, Silahkan Dicoba Kembali";
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (objH.nmPeserta != null)
            {
                objKepesertaan.data = objH;
                objM.code = "200"; objM.message = "OK";
                objKepesertaan.pesan = objM;
            }
            else
            {
                objKepesertaan.data = null;
                objM.code = "302"; objM.message = "Peserta Tidak Terdaftar";
                objKepesertaan.pesan = objM;
            }

            return objKepesertaan;
        }
        #endregion

        #region cekva
        public responseVAKepesertaanObj cekVA(string nokartu)
        {
            responseVAKepesertaanObj objKepesertaan = new responseVAKepesertaanObj();
            dataVAKepesertaanObj objH = new dataVAKepesertaanObj();

            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conKepser);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "JENI_GETPESERTA_BY_NOKA_NIK";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            try
            {
                SqlDataReader drH;
                con.Open();

                cmdH.Parameters.Add(new SqlParameter("@CODE", "4"));
                cmdH.Parameters.Add(new SqlParameter("@NOKA", nokartu));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    if (drH["VA"] != DBNull.Value) { objH.noVA = drH["VA"].ToString(); }
                }

                drH.Close();
                cmdH.Parameters.Clear();
            }
            catch
            {
                objM.code = "401"; objM.message = "Transaksi Tidak Dapat Diproses, Silahkan Dicoba Kembali";
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            if (objH.noVA != null)
            {
                objKepesertaan.data = objH;
                objM.code = "200"; objM.message = "Ok";
                objKepesertaan.pesan = objM;
            }
            else
            {
                objKepesertaan.data = null;
                objM.code = "304"; objM.message = "Jenis Peserta Bukan PBPU atau BP";
                objKepesertaan.pesan = objM;
            }

            return objKepesertaan;
        }
        #endregion

        #region cekanggotakeluarga
        public responseAnggotaKeluargaKepesertaanObj cekAnggotaKeluarga(string nokartu)
        {
            responseAnggotaKeluargaKepesertaanObj objKepesertaan = new responseAnggotaKeluargaKepesertaanObj();
            List<dataAnggotaKeluargaKepesertaanObj> Lobj = new List<dataAnggotaKeluargaKepesertaanObj>();
            metaData objM = new metaData();

            SqlConnection con = new SqlConnection(conKepser);
            SqlCommand cmdH = new SqlCommand();

            cmdH.CommandText = "JENI_GETPESERTA_BY_NOKA_NIK";

            cmdH.CommandType = CommandType.StoredProcedure;
            cmdH.Connection = con;

            try
            {
                SqlDataReader drH;
                con.Open();

                cmdH.Parameters.Add(new SqlParameter("@CODE", "2"));
                cmdH.Parameters.Add(new SqlParameter("@NOKA", nokartu));

                drH = cmdH.ExecuteReader();
                while (drH.Read())
                {
                    dataAnggotaKeluargaKepesertaanObj objH = new dataAnggotaKeluargaKepesertaanObj();

                    if (drH["NOKAPST_KEPALA"] != DBNull.Value) { objH.nokapstKepalaKeluarga = drH["NOKAPST_KEPALA"].ToString(); }
                    if (drH["NMPST_KEPALA"] != DBNull.Value) { objH.nmKepalaKeluarga = drH["NMPST_KEPALA"].ToString(); }
                    if (drH["JKPST_KEPALA"] != DBNull.Value) { objH.jnskelKepalaKeluarga = drH["JKPST_KEPALA"].ToString(); }
                    if (drH["HUBKEL_KEPALA"] != DBNull.Value) { objH.nmHubKelKepalaKeluarga = drH["HUBKEL_KEPALA"].ToString(); }
                    if (drH["NMPPK_KEPALA"] != DBNull.Value) { objH.nmPPKKepalaKeluarga = drH["NMPPK_KEPALA"].ToString(); }
                    if (drH["NOKAPST_ANGGOTA"] != DBNull.Value) { objH.nokapstAgtKeluarga = drH["NOKAPST_ANGGOTA"].ToString(); }
                    if (drH["NMPST_ANGGOTA"] != DBNull.Value) { objH.nmAgtKeluarga = drH["NMPST_ANGGOTA"].ToString(); }
                    if (drH["JKPST_ANGGOTA"] != DBNull.Value) { objH.jnskelAgtKeluarga = drH["JKPST_ANGGOTA"].ToString(); }
                    if (drH["NMHUBKEL_ANGGOTA"] != DBNull.Value) { objH.nmHubKelAgtKeluarga = drH["NMHUBKEL_ANGGOTA"].ToString(); }
                    if (drH["NMPPK_ANGGOTA"] != DBNull.Value) { objH.nmPPKAgtKeluarga = drH["NMPPK_ANGGOTA"].ToString(); }
                    if (drH["NMSTATUSPST"] != DBNull.Value) { objH.nmStatusAgtKeluarga = drH["NMSTATUSPST"].ToString(); }
                    Lobj.Add(objH);
                    //no = no + 1;
                }

                drH.Close();
                cmdH.Parameters.Clear();

                if (Lobj.Count>0)
                {
                    objM.code = "200"; objM.message = "Ok";
                }
                else
                {
                    objM.code = "302"; objM.message = "Peserta Tidak Terdaftar";
                }
            }
            catch
            {
                objM.code = "401"; objM.message = "Transaksi Tidak Dapat Diproses, Silahkan Dicoba Kembali";
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            objKepesertaan.pesan = objM;
            objKepesertaan.data = Lobj;
            return objKepesertaan;
        }
        #endregion
    }
}
