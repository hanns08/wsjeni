﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace objectLib
{
    [DataContract]
    public class ResData<T>
    {
        [DataMember]
        private T response;

        [DataMember]
        private metaData metaData;

        public ResData()
        {
            this.metaData = new metaData();
        }


        public metaData getMetaData()
        {
            return metaData;
        }

        public void setMetaData(metaData meta)
        {
            this.metaData = meta;
        }

        public T getResponse()
        {
            return response;
        }

        public void setResponse(T pst)
        {
            this.response = pst;
        }

    }
}
