﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace objectLib
{
    public class dataBaseKepesertaanObj
    {
        public string kdjnspeserta { get; set; }
        public string nokapst { get; set; }
    }

    public class responseBaseKepesertaanObj
    {
        
        public dataBaseKepesertaanObj data { get; set; }
        public metaData pesan { get; set; }
    }

    [DataContract]
    public class dataStatusKepesertaanObj
    {
        [DataMember(Order = 1)]
        public string nmPeserta { get; set; }
        [DataMember(Order = 2)]
        public string jnskelPeserta { get; set; }
        [DataMember(Order = 3)]
        public string tglLhrPeserta { get; set; }
        [DataMember(Order = 4)]
        public string nmStatusPeserta { get; set; }
        [DataMember(Order = 5)]
        public string jnsPeserta { get; set; }
        [DataMember(Order=6)]
        public string klsPeserta { get; set; }
        [DataMember(Order = 7)]
        public string nmPPK { get; set; }
    }

    public class responseStatusKepesertaanObj
    {
        public dataStatusKepesertaanObj data { get; set; }
        public metaData pesan { get; set; }
    }

    [DataContract]
    public class dataAnggotaKeluargaKepesertaanObj
    {
        [DataMember(Order = 1)]
        public string nokapstKepalaKeluarga { get; set; }
        [DataMember(Order = 2)]
        public string nmKepalaKeluarga { get; set; }
        [DataMember(Order = 3)]
        public string jnskelKepalaKeluarga { get; set; }
        [DataMember(Order = 4)]
        public string nmHubKelKepalaKeluarga { get; set; }
        [DataMember(Order = 5)]
        public string nmPPKKepalaKeluarga { get; set; }
        [DataMember(Order = 6)]
        public string nokapstAgtKeluarga { get; set; }
        [DataMember(Order = 7)]
        public string nmAgtKeluarga { get; set; }
        [DataMember(Order=8)]
        public string jnskelAgtKeluarga { get; set; }
        [DataMember(Order=9)]
        public string nmHubKelAgtKeluarga { get; set; }
        [DataMember(Order = 10)]
        public string nmPPKAgtKeluarga { get; set; }
        [DataMember(Order =11)]
        public string nmStatusAgtKeluarga { get; set; }
    }

    public class listAnggotaKeluargaKepesertaanObj
    {
        public List<dataAnggotaKeluargaKepesertaanObj> list { get; set; }
    }

    public class responseAnggotaKeluargaKepesertaanObj
    {
        public List<dataAnggotaKeluargaKepesertaanObj> data { get; set; }
        public metaData pesan { get; set; }
    }

    public class dataVAKepesertaanObj
    {
        public string noVA { get; set; }
    }

    public class responseVAKepesertaanObj
    {
        public dataVAKepesertaanObj data { get; set; }
        public metaData pesan { get; set; }
    }
}
