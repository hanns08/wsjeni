﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace objectLib
{
    class absensiObj
    {
    }

    #region absensi
    [DataContract]
    public class dataAbsensi
    {
        [DataMember(Order = 1)]
        public string npp { get; set; }
        [DataMember(Order = 2)]
        public string nama { get; set; }
        [DataMember(Order = 3)]
        public string absenmasuk { get; set; }
        [DataMember(Order = 4)]
        public string absenmasukdari { get; set; }
        [DataMember(Order = 5)]
        public string absenpulang { get; set; }
        [DataMember(Order = 6)]
        public string absenpulangdari { get; set; }

    }

    public class responseDataAbsensiObj
    {
        public dataAbsensi data { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion
}
