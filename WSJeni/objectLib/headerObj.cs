﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace objectLib
{
    public class headerObj
    {
        public string consId { get; set; }
        public string signature { get; set; }
        public string consPwd { get; set; }
        public string timestamp { get; set; }
        public string contenttype { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string token { get; set; }
        public string kdPpk { get; set; }
        public string methodid { get; set; }
    }


    public class responseHeaderObj
    {
        public headerObj header { get; set; }
        public metaData pesan { get; set; }
    }
}
