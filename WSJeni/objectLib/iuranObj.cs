﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace objectLib
{
    
    [DataContract]
    public class dataIuranObj
    {
        [DataMember(Order = 1)]
        public string premi { get; set; }
        [DataMember(Order =2)]
        public string status { get; set; }
    }

    public class responseIuranObj
    {
        public dataIuranObj data { get; set; }
        public metaData pesan { get; set; }
    }
}
