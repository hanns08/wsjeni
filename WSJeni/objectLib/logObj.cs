﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace objectLib
{
    public class logObj
    {
        public string webservice { get; set; }
        public string request_from { get; set; }
        public string method { get; set; }
        public string methodId { get; set; }
        public string header { get; set; }
        public string url { get; set; }
        public string request { get; set; }
        public string response { get; set; }

    }
}
