﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace objectLib
{
    #region statustiket
    [DataContract]
    public class dataStatusTiket
    {
        [DataMember(Order = 1)]
        public string kodetiket { get; set; }
        [DataMember(Order = 2)]
        public string judultiket { get; set; }
        [DataMember(Order = 3)]
        public string deskripsi { get; set; }
        [DataMember(Order = 4)]
        public string aplikasi { get; set; }
        [DataMember(Order = 5)]
        public string kategori { get; set; }
        [DataMember(Order = 6)]
        public string subkategori { get; set; }
        [DataMember(Order = 7)]
        public string tanggalbuat { get; set; }
        [DataMember(Order = 8)]
        public string status { get; set; }
        [DataMember(Order = 9)]
        public string catatan { get; set; }
        [DataMember(Order = 10)]
        public string posisitiket { get; set; }
        [DataMember(Order = 11)]
        public string user { get; set; }
        [DataMember(Order = 12)]
        public string unitkerja { get; set; }
        [DataMember(Order = 13)]
        public string email { get; set; }
    }

    public class responseStatusTiketObj
    {
        public dataStatusTiket data { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion

    #region eskalasiTiket
    [DataContract]
    public class dataEskalasiTiket
    {
        [DataMember(Order = 1)]
        public string kodetiket { get; set; }
        [DataMember(Order = 2)]
        public string dari { get; set; }
        [DataMember(Order = 3)]
        public string ke { get; set; }
        [DataMember(Order = 4)]
        public string catataneskalasi { get; set; }
        [DataMember(Order = 5)]
        public string tanggaleskalasi { get; set; }
    }

    public class dataResponseEskalasiTiket
    {
        public List<dataEskalasiTiket> list { get; set; }
    }

    public class responseEskalasiTiket
    {
        public dataResponseEskalasiTiket list { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion

    #region penangananTiket
    [DataContract]
    public class dataPenangananTiket
    {
        [DataMember(Order = 1)]
        public string kodetiket { get; set; }
        [DataMember(Order = 2)]
        public string user { get; set; }
        [DataMember(Order = 3)]
        public string unitkerja { get; set; }
        [DataMember(Order = 4)]
        public string deskripsipenanganan { get; set; }
        [DataMember(Order = 5)]
        public string tanggalpenanganan { get; set; }
    }

    public class dataResponsePenanganganTiket
    {
        public List<dataPenangananTiket> list { get; set; }
    }

    public class responsePenangananTiket
    {
        public dataResponsePenanganganTiket list { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion

    #region statusBMC
    [DataContract]
    public class dataStatusBMC
    {
        [DataMember(Order = 1)]
        public string kodeuser { get; set; }
        [DataMember(Order = 2)]
        public string kodeeskalasi { get; set; }
        [DataMember(Order = 3)]
        public string judultiket { get; set; }
        [DataMember(Order = 4)]
        public string deskripsi { get; set; }
        [DataMember(Order = 5)]
        public string aplikasi { get; set; }
        [DataMember(Order = 6)]
        public string kategori { get; set; }
        [DataMember(Order = 7)]
        public string subkategori { get; set; }
        [DataMember(Order = 8)]
        public string tanggalbuat { get; set; }
        [DataMember(Order = 9)]
        public string status { get; set; }
        [DataMember(Order = 10)]
        public string catatan { get; set; }
        [DataMember(Order = 11)]
        public string posisitiket { get; set; }
        [DataMember(Order = 12)]
        public string user { get; set; }
        [DataMember(Order = 13)]
        public string unitkerja { get; set; }
        [DataMember(Order = 14)]
        public string email { get; set; }
    }

    public class dataResponseStatusBMC
    {
        public List<dataStatusBMC> list { get; set; }
    }

    public class responseStatusBMCObj
    {
        public dataResponseStatusBMC list { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion

    #region eskalasiBMC
    [DataContract]
    public class dataEskalasiBMC
    {
        [DataMember(Order = 1)]
        public string kodetiket { get; set; }
        [DataMember(Order = 2)]
        public string kepemilikan { get; set; }
        /*[DataMember(Order = 3)]
        public string ke { get; set; }*/
        /*[DataMember(Order = 3)]
        public string catataneskalasi { get; set; }*/
        [DataMember(Order = 3)]
        public string tanggaleskalasi { get; set; }
        [DataMember(Order = 4)]
        public string waktupenanganan { get; set; }
    }

    public class dataResponseEskalasiBMC
    {
        public List<dataEskalasiBMC> list { get; set; }
    }

    public class responseEskalasiBMC
    {
        public dataResponseEskalasiBMC list { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion

    #region handlingtiket
    [DataContract]
    public class dataHandlingTiket
    {
        [DataMember(Order = 1)]
        public string kodetiket { get; set; }
        [DataMember(Order = 2)]
        public string dari { get; set; }
        [DataMember(Order = 3)]
        public string deskripsipenanganan { get; set; }
        [DataMember(Order = 4)]
        public string tanggal { get; set; }        
    }

    public class dataResponseHandlingTiket
    {
        public List<dataHandlingTiket> list { get; set; }
    }

    public class responseHandlingTiketObj
    {
        public dataResponseHandlingTiket list { get; set; }
        public metaData pesan { get; set; }
    }
    #endregion
}
