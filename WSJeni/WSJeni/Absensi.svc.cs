﻿using System;
using System.IO;
using objectLib;
using Newtonsoft.Json.Linq;
using modelLib;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Absensi" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Absensi.svc or Absensi.svc.cs at the Solution Explorer and start debugging.
    public class Absensi : IAbsensi
    {
        public void DoWork()
        {
        }

        #region cekabsensi
        public object cekAbsensi(string npp)
        {
            ResData<dataAbsensi> objRes = new ResData<dataAbsensi>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8026);
            dataAbsensi objL = new dataAbsensi();
            ResData<string> objResE = new ResData<string>();
            responseHeaderObj objR = new responseHeaderObj();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            int tahun = DateTime.Today.Year;
            int bulan = DateTime.Today.Month;
            int tanggal = DateTime.Today.Day;

            if (objM.code.Equals("200"))
            {
                if (npp.Length != 5)
                {
                    objM.code = "201";
                    objM.message = "NPP harus 5 karakter";
                    objRes.setMetaData(objM);
                    objRes.setResponse(null);

                    //log

                    objR = GlobalUtils.getConsIdSecretkey(8026);
                    objLog = GlobalUtils.getInfoService();
                    objLog.methodId = objR.header.methodid;
                    objLog.request_from = objR.header.kdPpk;
                    objLog.response = JsonConvert.SerializeObject(objM);
                    objMLog = clsLog.update_Log(objLog);

                    //end log

                    return objRes;
                }

                objM.code = "201";
                objM.message = "Laporan Absensi dapat dilihat pada IHC Bestamo";

                //responseDataAbsensiObj objData = new responseDataAbsensiObj();
                //absensiCls cls = new absensiCls();
                //objData = cls.cekAbsensi(npp, tahun, bulan, tanggal);
                //objM = objData.pesan;

                //if (objM.code.Equals("200"))
                //{

                //    objL = objData.data;
                //    //objRes.setResponse(objL);
                //}
                //else
                //{
                //    objM.code = objM.code;
                //    objM.message = objM.message;
                //}

            }

            //log

            objR = GlobalUtils.getConsIdSecretkey(8026);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            //if (GlobalUtils.strEncrypted.Equals("0"))
            //{
            //    if (objM.code.Equals("200"))
            //    {
            //        //objRes.setResponse(objL);
            //        //objRes.setMetaData(objM);

            //        objM.code = "201";
            //        objM.message = "Laporan Absensi dapat dilihat pada IHC Bestamo";
            //        objRes.setResponse(null);
            //        objRes.setMetaData(objM);
            //    }
            //    else
            //    {
            //        //objRes.setResponse(null);
            //        //objRes.setMetaData(objM);

            //        objM.code = "201";
            //        objM.message = "Laporan Absensi dapat dilihat pada IHC Bestamo";
            //        objRes.setResponse(null);
            //        objRes.setMetaData(objM);
            //    }

            //    return objRes;
            //}
            //else
            //{
            //    if (objL.nama != null)
            //    {
            //        //Encrypted
            //        //string LZ = GlobalUtils.LZStringCompress(objL);
            //        //SecurityController _security = new SecurityController();
            //        //string LZEncrypted = _security.Encrypt(objR, LZ);
            //        //objResE.setResponse(LZEncrypted);

            //        objM.code = "201";
            //        objM.message = "Laporan Absensi dapat dilihat pada IHC Bestamo";
            //        objRes.setResponse(null);
            //        objRes.setMetaData(objM);

            //    }

            //    objResE.setMetaData(objM);
            //    //objRes.setMetaData(objM);

            //    return objResE;
            //    //return objRes;
            //}

            objRes.setResponse(null);
            objRes.setMetaData(objM);

            return objRes;
        }
        #endregion
    }
}
