﻿using modelLib;
using objectLib;
using System;
using System.Configuration;
using System.Net;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Script.Serialization;

namespace WSJeni
{
    public class GlobalUtils
    {
        public static string strEncrypted = ConfigurationManager.AppSettings["isEncrypted"];

        public static string Generate(string sConsumerId, string sPassword, string sTimeStamp)
        {
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(sPassword);
            string message = sConsumerId + "&" + sTimeStamp;
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }


        public static metaData GetSigIn(int methodId)
        {
            metaData obj = new metaData();
            var request = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            WebHeaderCollection headers = request.Headers;
            headerObj objH = new headerObj();

            objH.consId = request.Headers["X-cons-id"];
            objH.timestamp = request.Headers["X-timestamp"];
            objH.signature = request.Headers["X-signature"];

            bool timestamp = isValidTimeStamp(long.Parse(objH.timestamp));
            if (timestamp)
            {
                try
                {
                    consIDValidasi cls = new consIDValidasi();
                    responseHeaderObj objR = cls.validasi(int.Parse(objH.consId), methodId);
                    if (objR.pesan.code.Equals("200"))
                    {
                        string conID = objR.header.consId;
                        string conPWD = objR.header.signature;
                        string sign = Generate(conID, conPWD, objH.timestamp);

                        if (sign.Equals(objH.signature))
                        {
                            obj.code = "200";
                            obj.message = "OK";

                        }
                        else
                        {
                            obj.code = "401";
                            obj.message = "Signature Service Tidak Sesuai";
                        }
                    }
                    else
                    {
                        if (objR.pesan.message.Equals("Access denied! You just reached your maximum daily limit access!"))
                        {
                            obj.code = "305";
                            obj.message = "Telah melewati batas akses untuk fitur ini";
                        }
                        else
                        {
                            obj.code = objR.pesan.code;
                            obj.message = objR.pesan.message;
                        }
                        
                    }
                }
                catch (Exception e)
                {
                    obj.code = "50000";
                    obj.message = e.Message.ToString();
                }

            }
            else
            {
                obj.code = "401";
                obj.message = "Expired Service";
            }

            return obj;
        }

        public static bool isValidTimeStamp(long timeClient)
        {
            DateTimeOffset dtOffset = new DateTimeOffset(DateTime.UtcNow, TimeSpan.FromHours(0));
            DateTime tStamp = (dtOffset.AddHours(7)).DateTime;

            long timeServer = ToTimeStamp(tStamp);
            long elapse = timeServer - timeClient;
            if (elapse < -420)
            { // jika waktu client selisih 7 menit terhadap server
                return false;
            }
            else if (elapse > 420)
            { // 7 menit limit untuk selisih timestamp
                return false;
            }
            else
            {
                return true;
            }
        }

        public static long ToTimeStamp(DateTime value)
        {
            long unixTimestamp = (long)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp;
        }


        public static bool isNumeric(string s)
        {
            int n = 0;
            return int.TryParse(s, out n);
        }

        public static bool dateFormatValidation(string s)
        {
            DateTime date = Convert.ToDateTime(s);

            string tempdate = date.ToString("yyyy-MM-dd");

            if (tempdate == s)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region getConsIDSecretKey
        public static responseHeaderObj getConsIdSecretkey(int methodId)
        {
            var request = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            WebHeaderCollection headers = request.Headers;
            headerObj objH = new headerObj();

            objH.consId = request.Headers["X-cons-id"];
            objH.timestamp = request.Headers["X-Timestamp"];
            objH.signature = request.Headers["X-signature"];

            responseHeaderObj objR = new responseHeaderObj();

            consIDValidasi cls = new consIDValidasi();
            objR = cls.validasi_2(int.Parse(objH.consId), methodId, objH.timestamp, objH.signature);

            return objR;
        }
        #endregion

        #region encrypt
        public static string LZStringCompress(object param)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = int.MaxValue;
            string dt = serializer.Serialize(param);

            return LZString.compressToEncodedURIComponent(dt);

        }

        public static string LZStringCompress_ConsId(object param, string consId, string secretKey)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = int.MaxValue;
            string dt = serializer.Serialize(param);

            return LZString.compressToEncodedURIComponent_ConsId(dt, consId, secretKey);

        }
        #endregion

        #region infoService
        public static logObj getInfoService()
        {
            var request = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            WebHeaderCollection headers = request.Headers;
            logObj objLog = new logObj();

            objLog.header = "X-cons-id:" + request.Headers["X-cons-id"] + " X-Timestamp:" + request.Headers["X-Timestamp"] + " X-signature:" + request.Headers["X-signature"];

            objLog.method = request.Method.ToString();
            objLog.url = HttpContext.Current.Request.Url.ToString();

            return objLog;
        }
        #endregion
    }
}