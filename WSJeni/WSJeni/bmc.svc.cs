﻿using System;
using System.IO;
using objectLib;
using Newtonsoft.Json.Linq;
using modelLib;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "bmc" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select bmc.svc or bmc.svc.cs at the Solution Explorer and start debugging.
    public class bmc : Ibmc
    {

        #region statusBMC
        public object statusBMC(string notiket)
        {
            ResData<dataResponseStatusBMC> objRes = new ResData<dataResponseStatusBMC>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8021);
            dataResponseStatusBMC objData = new dataResponseStatusBMC();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (!notiket.Length.Equals(15))
            {
                objM.code = "301";
                objM.message = "Kode Tiket Salah";
            }

            if (objM.code.Equals("200"))
            {
                responseStatusBMCObj objTiket = new responseStatusBMCObj();
                tiketCls cls = new tiketCls();
                objTiket = cls.cekStatusBMC(notiket);
                objM = objTiket.pesan;

                if (objM.code.Equals("200"))
                {

                    objData = objTiket.list;
                    //objRes.setResponse(objL);
                }
                else
                {
                    objM.code = objM.code;
                    objM.message = objM.message;
                }

            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8021);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objData);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objData.list != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objData);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        #region eskalasiBMC
        public object eskalasiBMC(string notiket)
        {
            ResData<dataResponseEskalasiBMC> objRes = new ResData<dataResponseEskalasiBMC>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8023);
            //dataEskalasiTiket objL = new dataEskalasiTiket();
            dataResponseEskalasiBMC objData = new dataResponseEskalasiBMC();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            
            if (objM.code.Equals("200"))
            {
      

                if (!notiket.Length.Equals(15))
                {
                    objM.code = "301";
                    objM.message = "Kode Tiket Salah";
                }

                if (objM.code.Equals("200"))
                {

                    responseEskalasiBMC objTiket = new responseEskalasiBMC();
                    tiketCls cls = new tiketCls();
                    objTiket = cls.getEskalasiBMC(notiket);
                    objM = objTiket.pesan;

                    if (objM.code.Equals("200"))
                    {

                        objData = objTiket.list;

                        objM.code = "200";
                        objM.message = "Ok";
                    }
                    else
                    {
                        objM.code = objM.code;
                        objM.message = objM.message;
                    }
                }


            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8023);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objData);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objData.list != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objData);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        #region handlingTiket
        public object handlingTiket(string notiket)
        {
            ResData<dataResponseHandlingTiket> objRes = new ResData<dataResponseHandlingTiket>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8022);
            dataResponseHandlingTiket objData = new dataResponseHandlingTiket();
            ResData<string> objResE = new ResData<string>();
            responseHeaderObj objR = new responseHeaderObj();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {
                /*if (notiket.Substring(0, 2) != "WO")
                {
                    objM.code = "201";
                    objM.message = "Kode tiket tidak sesuai";
                    objRes.setMetaData(objM);
                    objRes.setResponse(null);

                    //log

                    objR = GlobalUtils.getConsIdSecretkey(8022);
                    objLog = GlobalUtils.getInfoService();
                    objLog.methodId = objR.header.methodid;
                    objLog.request_from = objR.header.kdPpk;
                    objLog.response = JsonConvert.SerializeObject(objM);
                    objMLog = clsLog.update_Log(objLog);

                    //end log

                    return objRes;
                }*/

                /*if (notiket.Length < 5)
                {
                    objM.code = "201";
                    objM.message = "Kode tiket minimal 5 karakter";
                    objRes.setMetaData(objM);
                    objRes.setResponse(null);

                    //log

                    objR = GlobalUtils.getConsIdSecretkey(8022);
                    objLog = GlobalUtils.getInfoService();
                    objLog.methodId = objR.header.methodid;
                    objLog.request_from = objR.header.kdPpk;
                    objLog.response = JsonConvert.SerializeObject(objM);
                    objMLog = clsLog.update_Log(objLog);

                    //end log

                    return objRes;
                }*/

                if (!notiket.Length.Equals(15))
                {
                    objM.code = "301";
                    objM.message = "Kode Tiket Salah";
                }

                if (objM.code.Equals("200"))
                {
                    responseHandlingTiketObj objTiket = new responseHandlingTiketObj();
                    tiketCls cls = new tiketCls();
                    objTiket = cls.cekHandlingTiket(notiket);
                    objM = objTiket.pesan;

                    if (objM.code.Equals("200"))
                    {

                        objData = objTiket.list;
                        //objRes.setResponse(objL);
                    }
                    else
                    {
                        objM.code = objM.code;
                        objM.message = objM.message;
                    }
                }
                

            }

            //log
            
            objR = GlobalUtils.getConsIdSecretkey(8022);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objData);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objData.list != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objData);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion
    }
}
