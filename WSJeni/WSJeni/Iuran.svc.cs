﻿using System;
using System.IO;
using objectLib;
using Newtonsoft.Json.Linq;
using modelLib;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WSJeni
{
    
    public class Iuran : IIuran
    {

        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        #region cekiuran
        public object cekIuran(string nokartu)
        {
            ResData<dataIuranObj> objRes = new ResData<dataIuranObj>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8027);
            ResData<string> objResE = new ResData<string>();
            dataIuranObj objL = new dataIuranObj();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {
                // start validasi symbol
                if (nokartu.All(char.IsLetterOrDigit))
                {
                    // start validasi char
                    if (nokartu.All(char.IsDigit))
                    {
                        // start validasi nokartu
                        if (nokartu.Length.Equals(13) || nokartu.Length.Equals(16))
                        {
                            // start cek base kepesertaan
                            responseBaseKepesertaanObj objKepesertaan = new responseBaseKepesertaanObj();
                            kepesertaanCls clsKepser = new kepesertaanCls();
                            objKepesertaan = clsKepser.cekBasePeserta(nokartu);
                            objM = objKepesertaan.pesan;
                            if (objM.code.Equals("200"))
                            {

                                dataBaseKepesertaanObj objK = new dataBaseKepesertaanObj();
                                objK.kdjnspeserta = objKepesertaan.data.kdjnspeserta;
                                objK.nokapst = objKepesertaan.data.nokapst;
                                // start cek jenis peserta
                                if ((objK.kdjnspeserta.Equals("14")) || (objK.kdjnspeserta.Equals("23")) || (objK.kdjnspeserta.Equals("24")) || (objK.kdjnspeserta.Equals("25")))
                                {
                                    //core
                                    responseIuranObj objIuran = new responseIuranObj();
                                    iuranCls cls = new iuranCls();
                                    objIuran = cls.cekIuran(objK.nokapst);
                                    objM = objIuran.pesan;
                                    if (objM.code.Equals("200"))
                                    {
                                        
                                        objL = objIuran.data;
                                        //objRes.setResponse(objL);
                                    }
                                    else
                                    {
                                        objM.code = objM.code;
                                        objM.message = objM.message;
                                    }
                                }
                                // end cek jenis peserta
                                else
                                {
                                    objM.code = "304";
                                    objM.message = "Jenis Kepesertaan Bukan PBPU atau BP";
                                }

                            }
                            // end cek base kepesertaan
                            else
                            {
                                objM.code = objM.code;
                                objM.message = objM.message;
                            }
                        }
                        // end validasi nokartu
                        else
                        {
                            objM.code = "301";
                            objM.message = "Noka atau NIK tidak tidak lengkap atau salah";
                        }
                    }
                    // end validasi char
                    else
                    {
                        objM.code = "306";
                        objM.message = "Noka atau NIK harus angka";
                    }
                }
                // end validasi simbol
                else
                {
                    objM.code = "306";
                    objM.message = "Noka atau NIK harus angka";
                }
                
            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8027);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objL);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objL.status != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objL);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        // Add more operations here and mark them with [OperationContract]
    }
}
