﻿using objectLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IKepesertaan" in both code and config file together.
    [ServiceContract]
    public interface IKepesertaan
    {
        [WebInvoke
                 (Method = "GET",
                 BodyStyle = WebMessageBodyStyle.WrappedRequest,
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json,
                  UriTemplate = "/cekstatus/{nokartu}")
              ]
        //ResData<dataStatusKepesertaanObj> cekStatus(string nokartu);
        [ServiceKnownType(typeof(ResData<dataStatusKepesertaanObj>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object cekStatus(string nokartu);

        [WebInvoke
                 (Method = "GET",
                 BodyStyle = WebMessageBodyStyle.WrappedRequest,
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json,
                  UriTemplate = "/cekva/{nokartu}")
              ]
        //ResData<dataVAKepesertaanObj> cekVA(string nokartu);
        [ServiceKnownType(typeof(ResData<dataVAKepesertaanObj>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object cekVA(string nokartu);

        [WebInvoke
                 (Method = "GET",
                 BodyStyle = WebMessageBodyStyle.WrappedRequest,
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json,
                  UriTemplate = "/cekkeluarga/{nokartu}")
              ]
        //ResData<listAnggotaKeluargaKepesertaanObj> cekAnggotaKeluarga(string nokartu);
        [ServiceKnownType(typeof(ResData<listAnggotaKeluargaKepesertaanObj>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object cekAnggotaKeluarga(string nokartu);

    }
}
