﻿using objectLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WSJeni
{
    [ServiceContract]
    public interface IIuran
    {
        [WebInvoke
                 (Method = "GET",
                 BodyStyle = WebMessageBodyStyle.WrappedRequest,
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json,
                  UriTemplate = "/cekstatus/{nokartu}")
              ]
        //ResData<dataIuranObj> cekIuran(string nokartu);
        [ServiceKnownType(typeof(ResData<dataIuranObj>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object cekIuran(string nokartu);
    }
}
