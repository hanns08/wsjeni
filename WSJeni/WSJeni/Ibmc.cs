﻿using objectLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "Ibmc" in both code and config file together.
    [ServiceContract]
    public interface Ibmc
    {
        [WebInvoke
                 (Method = "GET",
                 BodyStyle = WebMessageBodyStyle.WrappedRequest,
                 RequestFormat = WebMessageFormat.Json,
                 ResponseFormat = WebMessageFormat.Json,
                  UriTemplate = "/status/{notiket}")
              ]
        //ResData<dataStatusKepesertaanObj> cekStatus(string nokartu);
        [ServiceKnownType(typeof(ResData<dataResponseStatusBMC>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object statusBMC(string notiket);

        [WebInvoke
                (Method = "GET",
                BodyStyle = WebMessageBodyStyle.WrappedRequest,
                RequestFormat = WebMessageFormat.Json,
                ResponseFormat = WebMessageFormat.Json,
                 UriTemplate = "/eskalasi/{notiket}")
             ]
        //ResData<dataStatusKepesertaanObj> cekStatus(string nokartu);
        [ServiceKnownType(typeof(ResData<dataResponseEskalasiBMC>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object eskalasiBMC(string notiket);

        [WebInvoke
                (Method = "GET",
                BodyStyle = WebMessageBodyStyle.WrappedRequest,
                RequestFormat = WebMessageFormat.Json,
                ResponseFormat = WebMessageFormat.Json,
                 UriTemplate = "/penanganan/{notiket}")
             ]
        //ResData<dataStatusKepesertaanObj> cekStatus(string nokartu);
        [ServiceKnownType(typeof(ResData<dataResponseHandlingTiket>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object handlingTiket(string notiket);
    }
}
