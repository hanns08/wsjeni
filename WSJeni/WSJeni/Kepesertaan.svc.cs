﻿using System;
using System.IO;
using objectLib;
using Newtonsoft.Json.Linq;
using modelLib;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Kepesertaan" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Kepesertaan.svc or Kepesertaan.svc.cs at the Solution Explorer and start debugging.
    public class Kepesertaan : IKepesertaan
    {
        #region cekanggotakeluarga
        public object cekAnggotaKeluarga(string nokartu)
        {
            ResData<listAnggotaKeluargaKepesertaanObj> objRes = new ResData<listAnggotaKeluargaKepesertaanObj>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8029);
            listAnggotaKeluargaKepesertaanObj objL = new listAnggotaKeluargaKepesertaanObj();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {
                // start validasi symbol
                if (nokartu.All(char.IsLetterOrDigit))
                {
                    // start validasi char
                    if (nokartu.All(char.IsDigit))
                    {
                        // start validasi kartu
                        if (nokartu.Length.Equals(13) || nokartu.Length.Equals(16))
                        {
                            responseAnggotaKeluargaKepesertaanObj objKepesertaan = new responseAnggotaKeluargaKepesertaanObj();
                            kepesertaanCls cls = new kepesertaanCls();
                            objKepesertaan = cls.cekAnggotaKeluarga(nokartu);
                            objM = objKepesertaan.pesan;
                            if (objM.code.Equals("200"))
                            {
                               
                                objL.list = objKepesertaan.data;
                                
                            }
                            else
                            {
                                objM.code = objM.code;
                                objM.message = objM.message;
                            }
                        }
                        // end validasi kartu
                        else
                        {
                            objM.code = "301";
                            objM.message = "Noka atau NIK tidak tidak lengkap atau salah";
                        }
                    }
                    // end validasi char
                    else
                    {
                        objM.code = "306";
                        objM.message = "Noka atau NIK harus angka";
                    }
                }
                // end validasi symbol
                else
                {
                    objM.code = "306";
                    objM.message = "Noka atau NIK harus angka";
                }
            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8029);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objL);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }
               
                return objRes;
            }
            else
            {
                if (objL.list != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objL);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        #region cekstatus
        public object cekStatus(string nokartu)
        {
            ResData<dataStatusKepesertaanObj> objRes = new ResData<dataStatusKepesertaanObj>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8028);
            dataStatusKepesertaanObj objL = new dataStatusKepesertaanObj();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {
                // start validasi symbol
                if (nokartu.All(char.IsLetterOrDigit))
                {
                    // start validasi nomor
                    if (nokartu.All(char.IsDigit))
                    {
                        // start validasi kartu
                        if (nokartu.Length.Equals(13) || nokartu.Length.Equals(16))
                        {
                            responseStatusKepesertaanObj objKepesertaan = new responseStatusKepesertaanObj();
                            kepesertaanCls cls = new kepesertaanCls();
                            objKepesertaan = cls.cekStatus(nokartu);
                            objM = objKepesertaan.pesan;

                            if (objM.code.Equals("200"))
                            {
                                
                                objL = objKepesertaan.data;
                                //objRes.setResponse(objL);
                            }
                            else
                            {
                                objM.code = objM.code;
                                objM.message = objM.message;
                            }
                        }
                        // end validasi kartu
                        else
                        {
                            objM.code = "301";
                            objM.message = "Noka atau NIK tidak tidak lengkap atau salah";
                        }
                    }
                    // end validasi nomor
                    else
                    {
                        objM.code = "306";
                        objM.message = "Noka atau NIK harus angka";
                    }
                }
                // end validasi symbol
                else
                {
                    objM.code = "306";
                    objM.message = "Noka atau NIK harus angka";
                }
            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8028);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objL);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objL.nmPeserta != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objL);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        #region cekva
        public object cekVA(string nokartu)
        {
            ResData<dataVAKepesertaanObj> objRes = new ResData<dataVAKepesertaanObj>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8030);
            ResData<string> objResE = new ResData<string>();
            dataVAKepesertaanObj objL = new dataVAKepesertaanObj();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {
                // start validasi symbol
                if (nokartu.All(char.IsLetterOrDigit))
                {
                    // start validasi char
                    if (nokartu.All(char.IsDigit))
                    {
                        // start validasi nokartu
                        if (nokartu.Length.Equals(13) || nokartu.Length.Equals(16))
                        {
                            // start cek base kepesertaan
                            responseBaseKepesertaanObj objKepesertaan = new responseBaseKepesertaanObj();
                            kepesertaanCls cls = new kepesertaanCls();
                            objKepesertaan = cls.cekBasePeserta(nokartu);
                            objM = objKepesertaan.pesan;
                            if (objM.code.Equals("200"))
                            {
                                dataBaseKepesertaanObj objK = new dataBaseKepesertaanObj();
                                objK.kdjnspeserta = objKepesertaan.data.kdjnspeserta;
                                objK.nokapst = objKepesertaan.data.nokapst;

                                // start cek jenis peserta
                                if ((objK.kdjnspeserta.Equals("14")) || (objK.kdjnspeserta.Equals("23")) || (objK.kdjnspeserta.Equals("24")) || (objK.kdjnspeserta.Equals("25")))
                                {
                                    //core
                                    responseVAKepesertaanObj objVA = new responseVAKepesertaanObj();
                                    objVA = cls.cekVA(objK.nokapst);
                                    objM = objVA.pesan;
                                    if (objM.code.Equals("200"))
                                    {
                                        
                                        objL = objVA.data;
                                        //objRes.setResponse(objL);
                                    }
                                    else
                                    {
                                        objM.code = objM.code;
                                        objM.message = objM.message;
                                    }
                                }
                                // end cek jenis peserta
                                else
                                {
                                    objM.code = "304";
                                    objM.message = "Jenis Kepesertaan Bukan PBPU atau BP";
                                }
                            }
                            // end cek base kepesertaan
                            else
                            {
                                objM.code = objM.code;
                                objM.message = objM.message;
                            }
                        }
                        // end validasi nokartu
                        else
                        {
                            objM.code = "301";
                            objM.message = "Noka atau NIK tidak tidak lengkap atau salah";
                        }
                    }
                    // end validasi char
                    else
                    {
                        objM.code = "306";
                        objM.message = "Noka atau NIK harus angka";
                    }
                }
                // end validasi simbol
                else
                {
                    objM.code = "306";
                    objM.message = "Noka atau NIK harus angka";
                }
            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8030);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objL);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objL.noVA != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objL);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion
    }
}
