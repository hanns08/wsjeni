﻿using System;
using System.IO;
using objectLib;
using Newtonsoft.Json.Linq;
using modelLib;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Tiket" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Tiket.svc or Tiket.svc.cs at the Solution Explorer and start debugging.
    public class Tiket : ITiket
    {
        
        #region cekstatus
        public object statusTiket(string notiket)
        {
            ResData<dataStatusTiket> objRes = new ResData<dataStatusTiket>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8021);
            dataStatusTiket objL = new dataStatusTiket();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {
                


                responseStatusTiketObj objTiket = new responseStatusTiketObj();
                tiketCls cls = new tiketCls();
                objTiket = cls.cekStatusTiket(notiket);
                objM = objTiket.pesan;

                if (objM.code.Equals("200"))
                {

                    objL = objTiket.data;
                    //objRes.setResponse(objL);
                }
                else
                {
                    objM.code = objM.code;
                    objM.message = objM.message;
                }
                       
            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8021);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objL);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objL.kodetiket != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objL);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        #region eskalasitiket
        public object eskalasiTiket(string notiket)
        {
            ResData<dataResponseEskalasiTiket> objRes = new ResData<dataResponseEskalasiTiket>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8023);
            //dataEskalasiTiket objL = new dataEskalasiTiket();
            dataResponseEskalasiTiket objData = new dataResponseEskalasiTiket();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {

                responseEskalasiTiket objTiket = new responseEskalasiTiket();
                tiketCls cls = new tiketCls();
                objTiket = cls.getEskalasiTiket(notiket);
                objM = objTiket.pesan;

                if (objM.code.Equals("200"))
                {
                    
                    objData = objTiket.list;

                    objM.code = "200";
                    objM.message = "Ok";
                }
                else
                {
                    objM.code = objM.code;
                    objM.message = objM.message;
                }

            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8023);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {
                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objData);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objData.list != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objData);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        #region penanganantiket
        public object penangananTiket(string notiket)
        {
            ResData<dataResponsePenanganganTiket> objRes = new ResData<dataResponsePenanganganTiket>();
            metaData objM = new metaData();
            objM = GlobalUtils.GetSigIn(8022);
            //dataEskalasiTiket objL = new dataEskalasiTiket();
            dataResponsePenanganganTiket objData = new dataResponsePenanganganTiket();
            ResData<string> objResE = new ResData<string>();

            //log
            string resLog = string.Empty;
            logCls clsLog = new logCls();
            metaData objMLog = new metaData();
            logObj objLog = new logObj();
            //end log

            if (objM.code.Equals("200"))
            {

                responsePenangananTiket objTiket = new responsePenangananTiket();
                tiketCls cls = new tiketCls();
                objTiket = cls.getPenangananTiket(notiket);
                objM = objTiket.pesan;

                if (objM.code.Equals("200"))
                {

                    objData = objTiket.list;

                    objM.code = "200";
                    objM.message = "Ok";
                }
                else
                {
                    objM.code = objM.code;
                    objM.message = objM.message;
                }

            }

            //log
            responseHeaderObj objR = new responseHeaderObj();
            objR = GlobalUtils.getConsIdSecretkey(8022);
            objLog = GlobalUtils.getInfoService();
            objLog.methodId = objR.header.methodid;
            objLog.request_from = objR.header.kdPpk;
            objLog.response = JsonConvert.SerializeObject(objM);
            objMLog = clsLog.update_Log(objLog);

            //end log

            // encrypt
            if (GlobalUtils.strEncrypted.Equals("0"))
            {

                if (objM.code.Equals("200"))
                {
                    objRes.setResponse(objData);
                    objRes.setMetaData(objM);
                }
                else
                {
                    objRes.setResponse(null);
                    objRes.setMetaData(objM);
                }

                return objRes;
            }
            else
            {
                if (objData.list != null)
                {
                    //Encrypted
                    string LZ = GlobalUtils.LZStringCompress(objData);
                    SecurityController _security = new SecurityController();
                    string LZEncrypted = _security.Encrypt(objR, LZ);
                    objResE.setResponse(LZEncrypted);

                }

                objResE.setMetaData(objM);
                //objRes.setMetaData(objM);

                return objResE;
                //return objRes;
            }
        }
        #endregion

        
    }
}
