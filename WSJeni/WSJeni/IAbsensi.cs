﻿using objectLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WSJeni
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAbsensi" in both code and config file together.
    [ServiceContract]
    public interface IAbsensi
    {
        [OperationContract]
        void DoWork();

        [WebInvoke
                (Method = "GET",
                BodyStyle = WebMessageBodyStyle.WrappedRequest,
                RequestFormat = WebMessageFormat.Json,
                ResponseFormat = WebMessageFormat.Json,
                 //UriTemplate = "/cekabsensi/npp/{npp}/timezone/{timezone}/platform/{platform}")
                 UriTemplate = "/check/{npp}")
             ]
        //ResData<dataStatusKepesertaanObj> cekStatus(string nokartu);
        [ServiceKnownType(typeof(ResData<dataAbsensi>))]
        [ServiceKnownType(typeof(ResData<string>))]

        object cekAbsensi(string npp);
    }
}
